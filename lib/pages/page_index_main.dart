import 'package:flutter/material.dart';

class PageIndexMain extends StatefulWidget {
  const PageIndexMain({super.key});

  @override
  State<PageIndexMain> createState() => _PageIndexMainState();
}

class _PageIndexMainState extends State<PageIndexMain> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
          backgroundColor: Colors.blue,
          title: Row(
            children: [
              Image.asset(
                'assets/images/logo.png',
                width: 50,
                alignment: Alignment.centerRight,
              ),
              const Text(
                '리뷰다있소',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                  shadows: [
                    Shadow(
                      blurRadius: 60.0,
                      color: Colors.white,
                      offset: Offset(2.0, 2.0),
                    ),
                  ],
                ),
                textAlign: TextAlign.center,
              )
            ],
          )),
    );
  }
}
